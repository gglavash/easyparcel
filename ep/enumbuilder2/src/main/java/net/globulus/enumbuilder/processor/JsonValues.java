package net.globulus.enumbuilder.processor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by gordanglavas on 29/09/16.
 */
@Target(value = ElementType.TYPE)
public @interface JsonValues {
}
