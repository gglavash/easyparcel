package net.globulus.easyparcel.processor.codegenerator.android;

import net.globulus.easyparcel.processor.codegenerator.AbsCodeGen;

/**
 * Bundle code generator
 *
 *
 */
public class BundleCodeGen extends AbsCodeGen {

  public BundleCodeGen() {
    super("Bundle");
  }
}
