package net.globulus.easyparcel.processor.util;

/**
 * Created by gordanglavas on 30/09/16.
 */
public final class ProcessorUtil {

	private ProcessorUtil() { }

	public static String getParcelerClassExtension() {
		return "_EasyParcelParceler";
	}
}
