package net.globulus.easyparcel.processor.codegenerator.android;

import net.globulus.easyparcel.processor.codegenerator.AbsCodeGen;

/**
 * SparseBooleanArray
 *
 *
 */
public class SparseBooleanCodeGen extends AbsCodeGen {

  public SparseBooleanCodeGen() {
    super("SparseBooleanArray");
  }
}
